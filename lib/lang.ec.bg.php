<?php

// All missing text declarations will fall back to English.

// Units (alphabetical order)
// Necessary for calculator units selectors
$ec_lang['u_depthFrac']="съотношение";
$ec_lang['u_depthPercent']="%";
$ec_lang['u_ft2']="ft^2";
$ec_lang['u_ft3ps']="cfs";
$ec_lang['u_ft']="ft";
$ec_lang['u_fth2o']="ft H2O";
$ec_lang['u_ftps']="ft/sec";
$ec_lang['u_gpm']="gpm";
$ec_lang['u_gradePercent']='% наклон';
$ec_lang['u_grade']="височина/дължина";
$ec_lang['u_in2']="sq. in.";
$ec_lang['u_inh2o']="in H2O";
$ec_lang['u_in']="in";
$ec_lang['u_knpcm2']="kN/cm^2";
$ec_lang['u_knpm2']="kN/m^2";
$ec_lang['u_kpa']="kPa";
$ec_lang['u_lps']="l/s";
$ec_lang['u_m2']="m^2";
$ec_lang['u_m3ps']="m^3/s";
$ec_lang['u_mgd']="MGD";
$ec_lang['u_mh2o']="m H2O";
$ec_lang['u_mld']="Ml/d";
$ec_lang['u_m']="m";
$ec_lang['u_mm2']="mm^2";
$ec_lang['u_mmh2o']="mm H2O";
$ec_lang['u_mm']="mm";
$ec_lang['u_mps']="m/s";
$ec_lang['u_npm2']="N/m^2";
$ec_lang['u_pa']="Pa";
$ec_lang['u_psf']="psf";
$ec_lang['u_psi']="psi";
$ec_lang['u_s']="sec";

// Page text
// In page order for easiest maintenance.
$ec_lang['menu_brand']='HawsEDC Калкулатори';
$ec_lang['menu_main_list']='Списък с калкулатори'; 
$ec_lang['menu_main_hydraulics']='Хидравлика'; 
$ec_lang['menu_main_language']='Език';
$ec_lang['template_welcome']='&gt;&gt; Drop your fears at the door; love is spoken here. Enjoy the <a href="https://hawsedc.com/download.php">free libre HawsEDC AutoCAD tools</a> too. &lt;&lt;';
$ec_lang['template_translation_help']='Можете ли да ми помогнете с превода, програмирането или хостинга за тези калкулатори?'; 
$ec_lang['template_feedback']='Моля споделете мнението си под формата на предложение или похвала. Този безплатен калкулатор надмина ли всичките ви очаквания?'; 
$ec_lang['template_printable_title']='Заглавие за принтиране'; 
$ec_lang['template_printable_subtitle']='Подзаглавие за принтиране'; 
$ec_lang['index_title']='Безплатни Oнлайн Инженерни Калкулатори'; 
$ec_lang['calc_set_units']='Set units:';
$ec_lang['calc_inputs']='Inputs';
$ec_lang['calc_results']='Results';
$ec_lang['view_hide_line']='[Hide this line]';
$ec_lang['view_printable']='Printable version (reload/refresh to restore)';
// Darcy-Weisbach. See mphl_ for missing text.
$ec_lang['dw_main_menu']='Darcy-Weisbach Pipe Head Loss';
$ec_lang['dw_main_title']='Free Online Darcy-Weisbach Pipe Head Loss Calculator';
$ec_lang['dw_main_desc']='Darcy-Weisbach Pipe Head Loss at Given Diameter, Roughness, and Flow';
$ec_lang['dw_roughness']='Darcy-Weisbach absolute roughness, e';
$ec_lang['dw_kinematic_viscosity']='Kinematic viscosity, v, in m<sup>2</sup>/sec (1E-6 for clean water at room temperature)';
$ec_lang['dw_reynolds_number']='Reynolds number, Re';
$ec_lang['dw_flow_regime']='Flow regime';
$ec_lang['dw_regime_laminar']='laminar';
$ec_lang['dw_regime_transitional']='transitional';
$ec_lang['dw_regime_turbulent']='turbulent';
$ec_lang['dw_friction_factor_method']='Friction factor method';
$ec_lang['dw_friction_factor']='Friction factor, f';
// Hazen-Williams. See mphl_ for missing text.
$ec_lang['hw_main_menu']='Hazen-Williams Pipe Head Loss';
$ec_lang['hw_main_title']='Free Online Hazen-Williams Pipe Head Loss Calculator';
$ec_lang['hw_main_desc']='Hazen-Williams Pipe Head Loss at Given Diameter, Roughness, and Flow';
$ec_lang['hw_roughness']='Hazen-Williams coefficient, C';
$ec_lang['calc_set_units']='Изберете мерни единици:'; 
$ec_lang['calc_results']='Резултати'; 
// Manning Irregular
$ec_lang['mi_menu']='Manning Irregular Channel'; //no
$ec_lang['mi_main_title']='Free Online Manning Irregular Channel Calculator'; //no
$ec_lang['mi_main_desc']='Irregular Channel Uniform Flow Calculator'; //no
$ec_lang['mi_waterSurfaceElevation']='Water surface elevation'; //no
$ec_lang['mi_q_sum']='Q by sum of segments'; //no
$ec_lang['mi_q_617']='Q by sum of regions with composite n per Chow 6-17 equal velocities'; //no
$ec_lang['mi_q_618']='Q by sum of regions with composite n per Chow 6-18 equilibrium of forces'; //no
$ec_lang['mi_xSecPoints']='Cross section points'; //no
$ec_lang['mi_station']='Sta'; //no
$ec_lang['mi_elevation']='Elev'; //no
$ec_lang['mi_q']='Q'; //no
$ec_lang['mi_n']='n<br />for seg-<br />ment'; //no
$ec_lang['mi_is_bank']='R<sub>h</sub>, Q<br />region<br />boundary<br />(Bank)'; //no
$ec_lang['mi_v']='v'; //no
$ec_lang['mi_t']='T'; //no
$ec_lang['mi_f']='f'; //no
$ec_lang['mi_d50_strickler']='Strickler<br />Implied<br />D<sub>50</sub>'; //no
$ec_lang['mi_d50_mc']='MC<br />Requ\'d.<br />D<sub>50</sub>'; //no
$ec_lang['mi_d50_mra']='MRA<br />Requ\'d.<br />D<sub>50</sub>'; //no
$ec_lang['mi_d50_searcy']='Searcy<br />Requ\'d.<br />D<sub>50</sub>'; //no
$ec_lang['mi_hv']='H<sub>v</sub>'; //no
$ec_lang['mi_tau']='Bot.<br />shear<br />&tau;'; //no
$ec_lang['mi_a']='A'; //no
$ec_lang['mi_pw']='P<sub>w</sub>'; //no
$ec_lang['mi_rh']='R<sub>h</sub>'; //no
$ec_lang['mi_notes']='Notes'; //no
$ec_lang['mi_notes_1_term']='Q Methods'; //no
$ec_lang['mi_notes_1_def']='Q by sum of conveyances underestimates the frictional contribution from steep segments. Q by composite n overestimates the effect of wide shallow (overbank) friction on flow in deeper areas (main channel).'; //no
$ec_lang['mi_notes_2_term']='D50 Values'; //no
$ec_lang['mi_notes_2_def']='The Strickler D<sub>50</sub> is the size (for a straight and clean channel) implied by the roughness entered. The other D<sub>50</sub> results are required to resist erosion per 1) Maricopa County and Robinson Rock Chutes, 2) Maynord, Ruff, and Abt (1989), and 3) Searcy (1967)'; //no
// Manning Pipe Flow
$ec_lang['mpf_main_menu']='Изчисления на Тръбопроводи по Манинг'; 
$ec_lang['mpf_main_title']='Безплатен Онлайн Калкулатор за Изчлиснения на Тръбопроводи по Манинг ';  
$ec_lang['mpf_main_desc']='Формула на Манинг за Равномерно Движение в Тръбопровод при Зададени Наклон И Воден Стълб'; 
$ec_lang['mpf_pipe_diameter']='Диаметър на тръбата, d<sub>0</sub>'; 
$ec_lang['mpf_manningRoughness']='Грапавина по Манинг, n'; 
$ec_lang['mpf_friction_slope']='Хидравличен наклон (често <a target="_blank" href="../pressureslope.php">?</a> равен на наклона на тръбата), J'; 
$ec_lang['mpf_depth_ratio']='Процентно запълване (или съотношение) към пълното запълване (100% или 1 при пълно запълване)'; 
$ec_lang['mpf_flow']='Водно количество, Q'; 
$ec_lang['mpf_velocity']='Скорост, V'; 
$ec_lang['mpf_velocity_head']='Скоростен напор, h<sub>v</sub>';  
$ec_lang['mpf_flow_area']='Водна площ'; 
$ec_lang['mpf_wetted_perimeter']='Мокър периметър'; 
$ec_lang['mpf_hydraulic_radius']='Хидравличен радиус'; 
$ec_lang['mpf_top_width']='Горна ширина, T'; 
$ec_lang['mpf_froude_number']='Число на Фруд, F'; 
$ec_lang['mpf_shear_stress']='Срязващo напрежение average (теглителна сила), tau'; 
// Manning Pipe Head Loss. See mpf_ for missing text.
$ec_lang['mphl_main_menu']='Хидравлични Загуби в Напорен Тръбопровод по Манинг'; 
$ec_lang['mphl_main_title']='Безплатен Онлайн Калкулатор: Хидравлични Загуби в Напорен Тръбопровод'; 
$ec_lang['mphl_main_desc']='Формула на Манинг за Хидравлични Загуби при Зададено Водно Количество'; 
$ec_lang['mphl_pipe_length']='Дължина на Тръбата, L'; 
$ec_lang['mphl_area']='площ, A';
$ec_lang['mphl_total_junction_k']='Общ Коефициент на Местните Съпротивления, ?'; 
$ec_lang['mphl_friction_loss']='Friction slope';
$ec_lang['mphl_friction_loss']='Загуби по дължина'; 
$ec_lang['mphl_junction_loss']='Местни загуби'; 
$ec_lang['mphl_total_loss']='Общи загуби'; 
$ec_lang['mphl_egl_1']='Downstream EGL';
$ec_lang['mphl_egl_2']='Upstream EGL';
$ec_lang['mphl_hgl_2']='Upstream HGL in pipe ' . $ec_lang['mpf_see_notes'];
// Manning Trapezoid. See mpf_ for missing text.
$ec_lang['mtc_menu']='Изчисления на Трапецовидни Канали по Манинг'; 
$ec_lang['mtc_main_title']='Безплатен Онлайн Калкулатор за Изчисления на Трапецовидни Канали по Манинг'; 
$ec_lang['mtc_main_desc']='Формула на Манинг за Равномерно Движение в Трапецовиден Канал при Зададени Наклон и Воден Стълб'; 
$ec_lang['mtc_bottom_width']='Ширина на Дъното'; 
$ec_lang['mtc_side_slope_1']='Страничен откос 1 (хориз./верт.)'; 
$ec_lang['mtc_side_slope_2']='Страничен откос 2 (хориз./верт.)'; 
$ec_lang['mtc_channel_slope']='Наклон на канала'; 
$ec_lang['mtc_flow_depth']='Воден стълб'; 
$ec_lang['mtc_bend_angle']='Bend Angle<a href="riprap-bend-angle.png" title="Click for image">?</a> (for riprap sizing)'; //No need
$ec_lang['mtc_sgrock']='Stone specific gravity (2.65)'; //No need
$ec_lang['mtc_d50_strickler']='Implied design <a href="javascript:alert(\'The Strickler formula relates n and D50 for a straight and clean channel. Adjust n so this implied design size is larger than the required erosion protection size. Then line your channel as implied.\')">?</a> riprap size based on n';
$ec_lang['mtc_d50_bottom']='Required bottom angular riprap size, D50, Maricopa County <a href="javascript:alert(\'For S0 < 2% per Isbash (1936) and Maricopa County, Arizona, US. For S >= 2% per Robinson Rock Chute \')">?</a>';
$ec_lang['mtc_d50_z1']='Required side slope 1 angular riprap size, D50, Maricopa County <a href="javascript:alert(\'For S0 < 2% per Isbash (1936) and Maricopa County, Arizona, US. For S >= 2% per Robinson Rock Chute \')">?</a>';
$ec_lang['mtc_d50_z2']='Required side slope 2 angular riprap size, D50, Maricopa County <a href="javascript:alert(\'For S0 < 2% per Isbash (1936) and Maricopa County, Arizona, US. For S >= 2% per Robinson Rock Chute \')">?</a>';
$ec_lang['mtc_d50_mra']='Required angular riprap size, D50, per Maynord, Ruff, and Abt (1989)'; //No need
$ec_lang['mtc_d50_searcy']='Required angular riprap size, D50, per Searcy (1967)'; //No need
// Robinson Rock Chute
$ec_lang['rrc_main_menu']="Бързоток Robinson Rock"; 
$ec_lang['rrc_main_desc']="Оразмеряване на бързоток по Robinson Rock - таблица"; 
// Weir Flow Simple
$ec_lang['ws_main_menu']='Хидравлични изчисления за преливник'; 
$ec_lang['ws_main_title']='Калкулатор за изчисляване на преливник с широк праг'; 
$ec_lang['ws_main_desc']='Калкулатор за изчисляване на преливник с широк праг'; 
$ec_lang['ws_weirLength']='Преливна широчина, b'; 
$ec_lang['ws_headWaterHeight']='Преливна височина, h'; 
$ec_lang['ws_weirCoefficient']='Коефициент на преливника, Cw'; 
$ec_lang['ws_notes_heading']='Забележки'; 
$ec_lang['ws_notes_we_term']='Уравнение на преливника'; 
// Weir Flow Irregular. See ws_ for missing text.
$ec_lang['wi_menu']='Хидравлика на преливници с неправилна геометрия'; 
$ec_lang['wi_main_title']='Free Online Segmented, Variable Depth, Irregular Broad-crested Weir Flow Calculator'; //no 
$ec_lang['wi_main_desc']='Irregular Broad-crested Weir Flow Calculator'; //no
$ec_lang['wi_headWaterelevation']='Headwater elevation';//no
$ec_lang['wi_weirPoints']='Weir points';//no
$ec_lang['wi_station']='Station';//no
$ec_lang['wi_elevation']='Elevation';//no
$ec_lang['wi_pondingHeight']='Ponding Height';//no
$ec_lang['wi_incrementalFlow']='Incremental Flow';//no
$ec_lang['wi_cumulativeFlow']='Cumulative Flow';//no
$ec_lang['wi_notes']='Notes';//no
$ec_lang['wi_notes_we_term']='Weir Equation';//no
$ec_lang['wi_notes_we_def']='q = if (length = 0) then 0 else if (slope=0) then cw*length*d0<sup>1.5</sup> else cw/(2.5*slope) * (d0<sup>2.5</sup> - d1<sup>2.5</sup>) where d1 and d0 are always positive or zero';//no
// Contact us.
$ec_lang['contact_title']="Контакт с HawsEDC"; 
$ec_lang['contactSendMessage']='Изпратете съобщение на Tom Haws'; 
$ec_lang['contactYourName']='Вашето име:'; 
$ec_lang['contactYourEmail']='Вашият e-mail адрес:'; 
$ec_lang['contactSubject']='Относно:'; 
$ec_lang['contact_message']='Съобщение:'; 
$ec_lang['contactSpamPrefix']='Five plus one equals'; //Shouldn't translate that
$ec_lang['contactSpamPostfix']='(Please spell it out. 1=one 2=two 3=three 4=four 5=five 6=six 7=seven +=plus 5+1=6)'; //Shouldn't translate that
$ec_lang['contactSubmitButton']='Изпратете съобщението'; 
