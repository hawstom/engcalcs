<?php

// All missing text declarations will fall back to English.


// Base words
// Do not use. Type entire page text phrase below.
$ec_lang['and']="et";
$ec_lang['area']="surface";
$ec_lang['bottom']="la base";
$ec_lang['calculate']="calculer";
$ec_lang['calculator']="calculateur";
$ec_lang['calculators']="$ec_lang[calculator]s";
$ec_lang['channel']="canal";
$ec_lang['coefficient']="coefficient";
$ec_lang['contents']="contenus";
$ec_lang['crest']="crête";
$ec_lang['cumulative']="total";
$ec_lang['depth']="profondeur";
$ec_lang['diameter']="diamètre";
$ec_lang['engineering']="ingenierie";
$ec_lang['engineers']="ingenieurs";
$ec_lang['flow']="circulation";
$ec_lang['formula']="formule";
$ec_lang['friction']="frottement";
$ec_lang['full']="plein";
$ec_lang['freeOnline']="gratuis en ligne";
$ec_lang['given']="donné";
$ec_lang['head']="pression de tête";
$ec_lang['headwater']="l'eau de source";
$ec_lang['height']="hauteur";
$ec_lang['horizontal']="horizontal";
$ec_lang['hydraulic']="hydraulique";
$ec_lang['hydraulics']="hydrauliques";
$ec_lang['incremental']="incrémental";
$ec_lang['irregular']="irregulier";
$ec_lang['junction']="embranchement";
$ec_lang['length']="longeur";
$ec_lang['loss']="perte";
$ec_lang['manning']="Manning";
$ec_lang['measurement']="mesure";
$ec_lang['of']="de";
$ec_lang['or']="ou";
$ec_lang['pipe']="tube";
$ec_lang['ponding']="pondant";
$ec_lang['points']="points";
$ec_lang['pressure']="pression";
$ec_lang['radius']="rayon";
$ec_lang['ratio']="rapport";
$ec_lang['results']="resultats";
$ec_lang['roughness']="rugosité";
$ec_lang['save']="sauve";
$ec_lang['side']="côté";
$ec_lang['slope']="pente";
$ec_lang['system']="système";
$ec_lang['to']="a";
$ec_lang['total']="total";
$ec_lang['trapezoidal']="trapezoidal";
$ec_lang['uniform']="uniforme";
$ec_lang['variable']="variable";
$ec_lang['velocity']="vitesse";
$ec_lang['vertical']="vertical";
$ec_lang['weir']="barrage";
$ec_lang['wettedperimeter']="périmètre mouiller";
$ec_lang['width']="largeur";

// Units (alphabetical order)
// Necessary for calculator units selectors
$ec_lang['u_depthFrac']="fraction";
$ec_lang['u_depthPercent']="%";
$ec_lang['u_fps']="ft/sec.";
$ec_lang['u_ft2']="ft^2";
$ec_lang['u_ft3ps']="ft^3/sec";
$ec_lang['u_ft']="ft";
$ec_lang['u_fth2o']="ft2O";
$ec_lang['u_ftps']="ft/sec";
$ec_lang['u_gpm']="galons/minute";
$ec_lang['u_grade']="pente";
$ec_lang['u_gradePercent']='% pente';
$ec_lang['u_in2']="inches^2";
$ec_lang['u_inches']="inches";
$ec_lang['u_inh2o']="in H2O";
$ec_lang['u_in']="in";
$ec_lang['u_knpcm2']="kN/cm^2";
$ec_lang['u_knpm2']="kN/m^2";
$ec_lang['u_kpa']="kPa";
$ec_lang['u_lps']="l/s";
$ec_lang['u_m2']="m^2";
$ec_lang['u_m3ps']="m^3/s";
$ec_lang['u_meters']="m";
$ec_lang['u_mgd']="millions de galons par jour";
$ec_lang['u_mh2o']="m H2O";
$ec_lang['u_mm2']="mm^2";
$ec_lang['u_mmh2o']="mm H2O";
$ec_lang['u_mm']="mm";
$ec_lang['u_mps']="m/s";
$ec_lang['u_npm2']="N/m^2";
$ec_lang['u_pa']="Pa";
$ec_lang['u_psf']="lbs/ft^2";
$ec_lang['u_psi']="lbs/in^2";
$ec_lang['u_s']="sec";

// Symbols
// Do not use. Type entire page text phrase below.
$ec_lang['s_d']="d";
$ec_lang['s_d0']="$ec_lang[s_d]<sub>0</sub>";
$ec_lang['s_hv']="h<sub>v</sub>";
$ec_lang['s_n']="n";
$ec_lang['s_q']="q";
$ec_lang['s_v']="v";

// Combined words
// Do not use. Type entire page text phrase below.
$ec_lang['bottomWidth']="$ec_lang[width] $ec_lang[of] $ec_lang[bottom]";
$ec_lang['channelSlope']="$ec_lang[slope] $ec_lang[of] $ec_lang[channel] ($ec_lang[vertical] / $ec_lang[horizontal])";
$ec_lang['flowCalculator']="$ec_lang[calculator] $ec_lang[of] $ec_lang[flow]";
$ec_lang['flowDepth']="$ec_lang[depth] $ec_lang[of] $ec_lang[flow]";
$ec_lang['frictionLoss']="$ec_lang[loss] de $ec_lang[friction]";
$ec_lang['junctionLoss']="$ec_lang[loss] de $ec_lang[junction]";
$ec_lang['totalLoss']="$ec_lang[loss] $ec_lang[total]e";
$ec_lang['manningFormula']="la $ec_lang[formula] $ec_lang[of] $ec_lang[manning]";
$ec_lang['sideSlope1']="$ec_lang[slope] $ec_lang[of] $ec_lang[side] 1 ($ec_lang[horizontal] / $ec_lang[vertical])";
$ec_lang['sideSlope2']="$ec_lang[slope] $ec_lang[of] $ec_lang[side] 2 ($ec_lang[horizontal] / $ec_lang[vertical])";

// Page text
// In page order for easiest maintenance.
$ec_lang['menu_brand']=ucfirst("$ec_lang[calculator]s HawsEDC");
$ec_lang['menu_main_list']=ucfirst("liste de $ec_lang[calculators]");
$ec_lang['menu_main_hydraulics']=ucfirst("$ec_lang[hydraulics]");
$ec_lang['menu_main_language']=ucfirst("langue");
$ec_lang['template_welcome']='&gt;&gt; Drop your fears at the door; love is spoken here. Enjoy the <a href="https://hawsedc.com/download.php">free libre HawsEDC AutoCAD tools</a> too. &lt;&lt;';
$ec_lang['template_translation_help']="Assistance a la traducion est desiree. Pouvez vous installer ce calculateur sur votre site?";
$ec_lang['template_feedback']="Donnez nous votre avis. Est-ce que cet ordinateur a depassé vos attentes?";
$ec_lang['template_printable_title']='Printable Title';
$ec_lang['template_printable_subtitle']='Printable Subtitle';
$ec_lang['index_title']=ucfirst("$ec_lang[calculators] $ec_lang[freeOnline] pour $ec_lang[engineers]");
$ec_lang['calc_set_units']='Set units:';
$ec_lang['calc_inputs']='Inputs';
$ec_lang['calc_results']=ucfirst("$ec_lang[results]:");
$ec_lang['view_hide_line']='[Hide this line]';
$ec_lang['view_printable']='Printable version (reload/refresh to restore)';
// Manning Pipe Flow
$ec_lang['mpf_main_menu']=ucfirst("$ec_lang[flow] $ec_lang[manning] en $ec_lang[pipe]s");
$ec_lang['mpf_main_title']=ucfirst("$ec_lang[calculator] $ec_lang[freeOnline] de $ec_lang[manningFormula] pour $ec_lang[flow] en $ec_lang[pipe]s");
$ec_lang['mpf_main_desc']=ucfirst("$ec_lang[flow] $ec_lang[uniform] $ec_lang[manning] en un $ec_lang[pipe] a $ec_lang[given] $ec_lang[slope] $ec_lang[and] $ec_lang[depth]");
$ec_lang['mpf_pipe_diameter']=ucfirst("$ec_lang[diameter] $ec_lang[of] $ec_lang[pipe], $ec_lang[s_d0]");
$ec_lang['mpf_manningRoughness']=ucfirst("$ec_lang[roughness] $ec_lang[manning], $ec_lang[s_n]");
$ec_lang['mpf_friction_slope']=ucfirst("$ec_lang[slope] de la
$ec_lang[pressure], S<sub>0</sub> (possiblement <a target=\"_blank\"
href=\"../pressureslope.php\">?</a> egale a $ec_lang[slope] de
$ec_lang[pipe])");
$ec_lang['mpf_depth_ratio']=ucfirst("$ec_lang[depth] de $ec_lang[flow] (1 ou 100% pour une tube pleine)");
$ec_lang['mpf_flow']=ucfirst("$ec_lang[flow], $ec_lang[s_q]");
$ec_lang['mpf_velocity']=ucfirst("$ec_lang[velocity], $ec_lang[s_v]");
$ec_lang['mpf_velocity_head']=ucfirst("$ec_lang[head] $ec_lang[of] la $ec_lang[velocity], $ec_lang[s_hv]");
$ec_lang['mpf_flow_area']=ucfirst("$ec_lang[area] de $ec_lang[flow]");
$ec_lang['mpf_wetted_perimeter']=ucfirst("$ec_lang[wettedperimeter]");
$ec_lang['mpf_hydraulic_radius']=ucfirst("$ec_lang[radius] $ec_lang[hydraulic]");
$ec_lang['mpf_top_width']=ucfirst("largeur supérieure, T");
$ec_lang['mpf_froude_number']=ucfirst("numéro de Froude, F");
$ec_lang['mpf_shear_stress']=ucfirst("Average tractive force, tau");
// Manning Pipe Head Loss. See mpf_ for missing text.
$ec_lang['mphl_main_menu']=ucfirst("$ec_lang[loss] $ec_lang[of] $ec_lang[head] en $ec_lang[pipe] $ec_lang[manning]");
$ec_lang['mphl_main_title']=ucfirst("$ec_lang[calculator] $ec_lang[freeOnline] de la $ec_lang[loss] de $ec_lang[head] en un $ec_lang[pipe] $ec_lang[manning]");
$ec_lang['mphl_main_desc']=ucfirst("la $ec_lang[loss] de $ec_lang[head] en un $ec_lang[pipe] $ec_lang[full]e, $ec_lang[given] la $ec_lang[flow]");
$ec_lang['mphl_pipe_length']=ucfirst("$ec_lang[length] $ec_lang[of] la tube");
$ec_lang['mphl_total_junction_k']="Coeficient total de $ec_lang[loss] dans les joints, k";
$ec_lang['mphl_friction_loss']=ucfirst("$ec_lang[frictionLoss]");
$ec_lang['mphl_junction_loss']=ucfirst("$ec_lang[junctionLoss]");
$ec_lang['mphl_total_loss']=ucfirst("$ec_lang[totalLoss]");
$ec_lang['mphl_egl_1']='Downstream EGL';
$ec_lang['mphl_egl_2']='Upstream EGL';
$ec_lang['mphl_hgl_2']='Upstream HGL in pipe ' . $ec_lang['mpf_see_notes'];
// Manning Trapezoid. See mpf_ for missing text.
$ec_lang['mtc_menu']=ucfirst("$ec_lang[channel] $ec_lang[trapezoidal] $ec_lang[manning]");
$ec_lang['mtc_main_title']=ucfirst("$ec_lang[calculator] $ec_lang[freeOnline] de $ec_lang[manningFormula] pour $ec_lang[channel] $ec_lang[trapezoidal]");
$ec_lang['mtc_main_desc']=ucfirst("$ec_lang[flow] $ec_lang[uniform] $ec_lang[manning] en un $ec_lang[channel] $ec_lang[trapezoidal] a $ec_lang[given] $ec_lang[slope] $ec_lang[and] $ec_lang[depth]");
$ec_lang['mtc_bottom_width']=ucfirst("$ec_lang[bottomWidth]");
$ec_lang['mtc_side_slope_1']=ucfirst("$ec_lang[sideSlope1]");
$ec_lang['mtc_side_slope_2']=ucfirst("$ec_lang[sideSlope2]");
$ec_lang['mtc_channel_slope']=ucfirst("$ec_lang[channelSlope]");
$ec_lang['mtc_flow_depth']=ucfirst("$ec_lang[flowDepth]");
$ec_lang['mtc_d50_bottom']='<span title="per Isbash (1936), Robinson, and Maricopa County, Arizona, US">Required bottom angular riprap size, D50, Maricopa County</span>';
$ec_lang['mtc_d50_mra']='Required angular riprap size, D50, per Maynord, Ruff, and Abt (1989)';
$ec_lang['mtc_d50_searcy']='Required angular riprap size, D50, per Searcy (1967)';
$ec_lang['mtc_d50_strickler']='<span title="per Strickler (Adjust n so this equals your design lining size)">Implied riprap size based on n</span>';
$ec_lang['mtc_d50_z1']='<span title="per Isbash (1936), Robinson, and Maricopa County, Arizona, US">Required side slope 1 angular riprap size, D50, Maricopa County</span>';
$ec_lang['mtc_d50_z2']='<span title="per Isbash (1936), Robinson, and Maricopa County, Arizona, US">Required side slope 2 angular riprap size, D50, Maricopa County</span>';
// Weir Flow Irregular. See ws_ for missing text.
$ec_lang['wi_menu']=ucfirst("$ec_lang[weir] $ec_lang[irregular]");
$ec_lang['wi_main_title']=ucfirst("$ec_lang[calculator] $ec_lang[freeOnline] para $ec_lang[weir] $ec_lang[irregular], de $ec_lang[depth] $ec_lang[variable] Broad-crested");
$ec_lang['wi_main_desc']=ucfirst("$ec_lang[calculator] para $ec_lang[weir] $ec_lang[irregular], de $ec_lang[depth] $ec_lang[variable] Broad-crested");
$ec_lang['wi_headWaterelevation']="Hauteur de l'eau de source, h";
$ec_lang['wi_weirPoints']=ucfirst("$ec_lang[points] $ec_lang[of] $ec_lang[weir]");
$ec_lang['wi_station']="Distancia";
$ec_lang['wi_elevation']="Nivel";
$ec_lang['wi_pondingHeight']=ucfirst("$ec_lang[height] $ec_lang[of] $ec_lang[ponding]");
$ec_lang['wi_incrementalFlow']=ucfirst("$ec_lang[flow] $ec_lang[incremental]");
$ec_lang['wi_cumulativeFlow']=ucfirst("$ec_lang[flow] $ec_lang[cumulative]");
$ec_lang['wi_notes_we_term']='Weir Equation';
$ec_lang['wi_notes_we_def']='q = if (length = 0) then 0 else if (slope=0) then cw*length*d0<sup>1.5</sup> else cw/(2.5*slope) * (d0<sup>2.5</sup> - d1<sup>2.5</sup>) where d1 and d0 are always positive or zero';
// Contact us.
$ec_lang['contact_title']="Contacte avec HawsEDC";
$ec_lang['contactSendMessage']='Envoiye un message a Tom Haws';
$ec_lang['contactYourName']='Votre nom:';
$ec_lang['contactYourEmail']='Votre email:';
$ec_lang['contactSubject']='Sujet:';
$ec_lang['contact_message']='Message:';
$ec_lang['contactSpamPrefix']='Five (cinque) plus one (un) equals (egale) ';
$ec_lang['contactSpamPostfix']='( S\'il vous plait, ecrivez les lettres en anglais. 1=one 2=two 3=three 4=four 5=five 6=six 7=seven +=plus 5+1=6)';
$ec_lang['contactSubmitButton']='contacter';
?>
