<?php

// All missing text declarations will fall back to English.

// Page text
// In page order for easiest maintenance.
$ec_lang['calc_set_units']='Alege unitate masură:';
$ec_lang['calc_results']='Rezultate';
// Manning Pipe Flow
$ec_lang['mpf_main_menu']='Manning Pipe Flow';
$ec_lang['mpf_main_title']='Free Online Manning Pipe Flow Calculator';
$ec_lang['mpf_main_desc']='Manning Formula Uniform Pipe Flow at Given Slope and Depth';
$ec_lang['mpf_pipe_diameter']='Diametru conductă';
$ec_lang['mpf_manningRoughness']='Rugozitate';
$ec_lang['mpf_friction_slope']='Panta (possibly <a target="_blank" href="../pressureslope.php">?</a> equal to pipe slope, S<sub>0</sub> ';
$ec_lang['mpf_depth_ratio']='Procent sau fracţie din conducta plină (100% sau 1 inseamnă plină)';
$ec_lang['mpf_flow']='Debit';
$ec_lang['mpf_velocity']='Viteză';
$ec_lang['mpf_velocity_head']='Presiunea dinamică';
$ec_lang['mpf_flow_area']='Secţiune de curgere';
$ec_lang['mpf_wetted_perimeter']='Perimetrul secţiunii de curgere';
$ec_lang['mpf_hydraulic_radius']='Raza hidraulică';
$ec_lang['mpf_top_width']='Laţimea suprafeţei libere';
$ec_lang['mpf_froude_number']='Numărul Froude';
$ec_lang['mpf_shear_stress']='Efort tangenţial promedio';